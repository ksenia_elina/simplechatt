<html>
<head><script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <style type="text/css">
    .zig-zag {
  background: #1ba1e2;
  position: relative;
}

.zig-zag:after {
  background: linear-gradient(-45deg, transparent 20px, #f8f9fa 0),
              linear-gradient( 45deg, transparent 20px, #f8f9fa 0);
  background-position: left bottom;
  background-size: 20px 20px;
  content: "";
  display: block;
  height: 20px;
  width: 100%;
  position: absolute;
}

.container
{
    overflow: hidden;
    padding: 1%;
}
  </style>
</head>
<body>
<header class="zig-zag bg-light text-dark">
<div class="container ">
 <div class="row">
    <div class="col-sm">
        <div id="info">
        Телефон:(499)340-9491</br>
        E-mail:info@future-group.ru
        </div>
    </div>
    <div class="col-sm">
    </div>
    <div class="col-sm">
        <div id="logo">
            <img src="./logo.jpg">
        </div>
    </div>
 </div>
 <h1> Комментарии </h1>   
</div>

</header>
<main class="bg-secondary text-white">
<div class="container">
<?php
        $host = 'http://'.$_SERVER['HTTP_HOST'];
        //var_dump(count($data));
        //$tags = explode(',', $row[3]);
        // Переменная хранит число сообщений выводимых на станице
        $num = 5;
        // Извлекаем из URL текущую страницу
        $page = $_GET['page'];
        $posts = count($data);
        // Определяем начало сообщений для текущей страницы
        $page = intval($page);
        // Находим общее число страниц
        $total = intval(($posts - 1) / $num) + 1;
        // Если значение $page меньше единицы или отрицательно
        // переходим на первую страницу
        // А если слишком большое, то переходим на последнюю
        if(empty($page) or $page < 0) $page = 1;
          if($page > $total) $page = $total;
        // Вычисляем начиная к какого номера
        // следует выводить сообщения
        $start = $page * $num - $num;
        $submass = array_slice($data, $start, $num);
	foreach($submass as $row)
	{
		//var_dump($submass[1]);
        echo '<div id="com">
        <div id="name">',
        $row[1],'   ', date("H:i d.m.Y", strtotime($row[2]))
        ,'</div>
        <div id="text">
        ',$row[3],'
        </div>
        <br>
        </div>
        ';
	}


?>
<div class="progress" style="height: 1px;">
  <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>
<h2> Оставить комментарий </h2> 
<form action="/" method="post" style="display: grid;width: 50%;">
    <label for="name">Ваше имя</label>
    <input type="text" name="name" id="name">

    <label for="comment">Ваш комментарий</label>
    <textarea name="comment" id="comment"></textarea>

    <button type="submit">Опубликовать</button>
</form>
<?php
    for($i=1; $i < $total+1; $i++)
    {
        echo '<a href="'.$host.'?page='.$i.'">'.$i.'</a>';
    }
    
?>
</p>
</div>
</main>
<footer class="bg-light text-dark">
<div class="container">
 <div class="row">
    <div class="col-sm">
        <div id="logo">
            <img src="logo.jpg">
        </div>
    </div>
        <div class="col-sm">
    </div>
    <div class="col-sm" style="padding-top: 15%;">
            <div id="info">
        Телефон:(499)340-9491</br>
        E-mail:info@future-group.ru</br>
        Адрес: Москва</br>
        © Все права защищены</br>
        </div>
    </div>
</div>
   
</div>
</footer>
</body>
</html>